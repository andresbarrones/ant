# This script create user with useradd command and default parameters

    # Clear
        clear
        echo " &"
        echo " └─· Runing script 'adduser.sh' "

    # Questions for customize user settings
        user_configuration_questions () {

        # Username
            user_username () {
                if [ -z $username ]; then
                    if [ -z $config_are_correct ]; then
                        read -p "   ├──· Name for new user → " username
                    else
                        read -p "   ├──· Name for new user [$username] → " username
                    fi

                    if [ -z $username ]; then
                        echo "      │  └── Username can't be blank "
                        user_username
                    else
                        # Enter SSH public key
                            read -p "   │  └── Paste public SSH key for $user (OpenSSH type) → " public_ssh_key
                    fi
                else           
                    # Enter SSH public key
                        echo "   ├──· Username → $username "
                        read -p "   │  └── Paste public SSH key for $user (OpenSSH type) → " public_ssh_key
                fi
            }
            user_username     

            # Default editor (nano, vi, vim, etc.)
                user_default_editor () {
                    if [ -z $config_are_correct ]; then
                        read -p "   ├──· (NANO - VIM - VI) Default editor → " default_editor
                    else
                        read -p "   ├──· (NANO - VIM - VI) Default editor [$default_editor] → " default_editor
                    fi

                    case $default_editor in
                        nano|NANO )
                            default_editor="nano"
                            echo "   │  └── $default_editor set as default editor " ;;
                        vim|VIM )
                            default_editor="vim"
                            echo "   │  └── $default_editor set as default editor " ;;
                        vi|VI )
                            default_editor="vi"
                            echo "   │  └── $default_editor set as default editor " ;;
                        * )
                            echo "   │  └── Sorry your answer isn't an option, please try again. "
                            user_default_editor
                    esac
                }
                user_default_editor

            # Colors in bash promt
                user_promt_color_config () {
                    if [ -z $config_are_correct ]; then
                        read -p "   ├──· (YES - NO) Do you like colors on the bash promt ? → " promt_color
                    else
                        read -p "   ├──· (YES - NO) Do you like colors on the bash promt ? [$promt_color] → " promt_color
                    fi

                    case $promt_color in
                        yes|y|YES|Y )
                            promt_color="yes"
                            echo "   │  └── Promt colors enabled " ;;
                        no|n|NO|N )
                            promt_color="no"
                            echo "   │  └── Promt colors isn't enabled " ;;
                        * )
                            echo "   │  └── Sorry your answer isn't an option, please try again. "
                            user_promt_color_config
                    esac
                }
                user_promt_color_config


            # Clock on bash promt
                user_promt_clock_config () {
                    if [ -z $config_are_correct ]; then
                        read -p "   ├──· (YES - NO) Do you like have a clock on the bash promt ? → " promt_clock
                    else
                        read -p "   ├──· (YES - NO) Do you like have a clock on the bash promt ? [$promt_clock] → " promt_clock
                    fi

                    case $promt_clock in
                        yes|y|YES|Y )
                            promt_clock="yes"
                            echo "   │  └── Promt clock enabled " ;;
                        no|n|NO|N )
                            promt_clock="no"
                            echo "   │  └── Promt clock isn't enabled " ;;
                        * )
                            echo "   │  └── Sorry your answer isn't an option, please try again. "
                            user_promt_clock_config
                    esac
                }
                user_promt_clock_config

            # sudo power like root
                user_sudo_power () {
                    if [ -z $config_are_correct ]; then
                        read -p "   ├──· (YES - NO) Can the new user run sudo like root ? → " sudo_power
                    else
                        read -p "   ├──· (YES - NO) Can the new user run sudo like root ? [$sudo_power] → " sudo_power
                    fi

                    case $sudo_power in
                        yes|y|YES|Y )
                            sudo_power="yes"
                            echo "   │  └── sudo power enabled " ;;
                        no|n|NO|N )
                            sudo_power="no"
                            echo "   │  └── sudo power isn't enabled " ;;
                        * )
                            echo "   │  └── Sorry your answer isn't an option, please try again. "
                            user_sudo_power
                    esac
                }
                user_sudo_power

            # sudo need password
                if [ $sudo_power = yes ]; then
                    user_sudo_password () {
                        if [ -z $config_are_correct ]; then
                            read -p "   ├──· (YES - NO) sudo like root need password for run it ? → " sudo_password
                        else
                            read -p "   ├──· (YES - NO) sudo like root need password for run it ? [$sudo_password] → " sudo_password
                        fi

                            case $sudo_password in
                                    yes|y|YES|Y )
                                        sudo_password="yes"
                                        echo "   │  └── sudo password enabled " ;;
                                    no|n|NO|N )
                                        sudo_password="no"
                                        echo "   │  └── sudo password don't needed " ;;
                                    * )
                                        echo "   │  └── Sorry your answer isn't an option, please try again. "
                                        user_sudo_password
                            esac
                        }
                        user_sudo_password
                fi


            # Configs are correct ?
                user_config_are_correct () {
                    read -p "   ├──· (YES - NO) Configs are correct ? → " config_are_correct
                    case $config_are_correct in
                        yes|y|YES|Y|"" )
                            echo "   ├── Perfect, we are creating the user ... " ;;
                        no|n|NO|N )
                            echo "   │  └── Running the configurator again ... "
                            user_configuration_questions ;;
                        * )
                            echo "   │  └── Sorry your answer isn't an option, please try again. "
                            user_config_are_correct
                    esac
                }
                user_config_are_correct
        }
        user_configuration_questions

    # Create user home dir
        echo "   ├── Creating user home folder ... "
        sudo mkdir /home/$username

    # Create empty authorized_keys file for new user
        echo "   ├── Creating user .ssh folder ... "
        sudo mkdir /home/$username/.ssh
        sudo touch /home/$username/.ssh/authorized_keys

    # Create new user with basic parameters
        echo "   ├── Creating user into system ... "
        sudo useradd -s /bin/bash -d /home/$username $username

    # Copy default user files (.profile, .bashrc) to user home folder
        echo "   ├── Creating user default files ... "
        sudo cp ../resources/.bashrc ../resources/.profile /home/$username/

    # Edit user files with previous configs
        # Change default_editor on .bashrc
            sudo sed -i "s/default_editor/$default_editor/g" "/home/$username/.bashrc"

        # Change promt_color on .basrc
            sudo sed -i "s/promt_color/$promt_color/g" "/home/$username/.bashrc"

        # Change promt_clock on .basrc
            sudo sed -i "s/promt_clock/$promt_clock/g" "/home/$username/.bashrc"

    # Adding SSH key to authorized_keys file
        echo "$public_ssh_key" > /home/$username/.ssh/authorized_keys
            
    # Set permissions to home folder for new user
        sudo chown -R $username:$username /home/$username
        echo "   ├──· Setting user permissions ... "

    # Add user to sudo groups if is required
        if [ $sudo_power = "yes" ]; then
            touch /etc/sudoers.d/$username
            printf "%s\n" "# $username can use sudo as root" "$username ALL=(ALL) ALL" > /etc/sudoers.d/$username
            echo "   │  └── Creating user file on /etc/sudoers.d/ ..."
        fi

    # Delete password requeriment for sudo command if "sudo_password = no"
        if [ $sudo_password = "no" ]; then
            rm /etc/sudoers.d/$username
            touch /etc/sudoers.d/$username
            printf "%s\n" "# $username don't need password to use sudo as root" "$username ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/$username
            echo "   │  └── Creating user file on /etc/sudoers.d/ ..."
        fi

    # End message
        echo "   │"
        echo "   └─ Script finished. "
        echo " "
